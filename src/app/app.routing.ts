import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { TopicsComponent } from './components/topics/topics.component';
import { TopicDetailComponent } from './components/topic-detail/topic-detail.component';
import { UserGuards } from './services/user.guard';
import { NoidentityGuards } from './services/noidentity.guard';
import { UsersComponent } from './components/users/users.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';


const appRoutes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'home', component: HomeComponent},
	{path: 'login', canActivate: [NoidentityGuards], component: LoginComponent},
	{path: 'register', canActivate: [NoidentityGuards], component: RegisterComponent},
	{path: 'settings', canActivate: [UserGuards], component: UserEditComponent},
	{path: 'topics', component: TopicsComponent },
	{path: 'topics/:page', component: TopicsComponent },
	{path: 'topic/:id', component: TopicDetailComponent},
	{path: 'users', component: UsersComponent},
	{path: 'profile/:id', component: ProfileComponent},
	{path: 'result/:search', component: SearchComponent},
	{path: '**', component: HomeComponent},
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);