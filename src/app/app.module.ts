import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFileUploaderModule } from 'angular-file-uploader';
import { MomentModule } from 'angular2-moment';
import { NgxHighlightJsModule } from '@nowzoo/ngx-highlight-js';

import { MyTopicsModule } from './my-topics/my-topics.module';

import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { TopicsComponent } from './components/topics/topics.component';
import { TopicDetailComponent } from './components/topic-detail/topic-detail.component';
import { UserGuards } from './services/user.guard';
import { UserService } from './services/user.service';
import { NoidentityGuards } from './services/noidentity.guard';
import { UsersComponent } from './components/users/users.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    UserEditComponent,
    TopicsComponent,
    TopicDetailComponent,
    UsersComponent,
    ProfileComponent,
    SearchComponent,
  ],
  imports: [
		BrowserModule,
		FontAwesomeModule,
    routing,
    FormsModule,
    HttpClientModule,
    AngularFileUploaderModule,
		MyTopicsModule,
		MomentModule,
		NgxHighlightJsModule.forRoot(),
  ],
  providers: [
		appRoutingProviders,
		UserGuards,
		UserService,
		NoidentityGuards
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

	constructor() {
    // Add an icon to the library for convenient access in other components
    library.add(fas, far);
  }
}
