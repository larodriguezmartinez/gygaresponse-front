import { Component, OnInit, DoCheck } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserService } from './services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { global } from './services/global';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ UserService ]
})
export class AppComponent implements OnInit, DoCheck{

   public identity: any;
   public token: string;;
   public pageTitle: string;
   public urlApi: string;
   public urlCloudinary: string;

   constructor(
      private titleService: Title,
      private _userService: UserService,
      private _router: Router,
      private _route: ActivatedRoute
   ) {
      this.pageTitle = 'GygaResponse';
      this.titleService.setTitle(this.pageTitle);
      this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
      this.urlApi = global.urlApi;
      this.urlCloudinary = global.urlCloudinary;

   }

   setTitle( newTitle: string) {
      this.titleService.setTitle( newTitle );
   }

   ngOnInit() {

   }

   ngDoCheck(){
      this.identity = this._userService.getIdentity();
   }

   logout(){
      localStorage.clear();
      this.identity = null;
      this.token = null;
      this._router.navigate(['/'])
   }
}

