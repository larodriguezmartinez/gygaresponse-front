import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { TopicService } from 'src/app/services/topic.service';
import { global } from 'src/app/services/global';
import { User } from 'src/app/models/user';
import { Topic } from 'src/app/models/topic';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css'],
	providers: [UserService,TopicService]
})
export class ProfileComponent implements OnInit {

	public pageTitle: string;
	public urlCloudinary: string;
	public topics: Topic[];
	public user: User;
	public status: string;

	constructor(
		private _userService: UserService,
		private _topicService: TopicService,
		private _router: Router,
		private _route: ActivatedRoute,
		private titleService: Title,

	) {
		this.pageTitle = 'Profile';
		this.titleService.setTitle(this.pageTitle);
		this.urlCloudinary = global.urlCloudinary;
	}

  ngOnInit() {

		this._route.params. subscribe( params => {
			var userId = params['id'];
			this.getUser(userId);
			this.getUserTopics(userId);
		});
  }

	getUser(userId){

		this._userService.getUser(userId).subscribe(
			response => {
				if(response.user){
					this.user = response.user;
				}
			},
			error => {
				this.status = 'error';
			}
		);

	}

	getUserTopics(userId){

		this._topicService.getUserTopics(userId).subscribe(
			response => {
				if(response.topics){
					this.topics = response.topics
				}
			},
			error => {
				this.status = 'error';
			}
		);
	}

}