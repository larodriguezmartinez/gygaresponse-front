import { Component, OnInit, DoCheck } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
Router
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ UserService ]
})
export class HomeComponent implements OnInit, DoCheck {

   public pageTitle: string;
	public identity: any;
	public search;

   constructor(
      private titleService: Title,
		private _userService: UserService,
		private _route: ActivatedRoute,
		private _router: Router,
   ) {
      this.pageTitle = 'GygaResponse';
      this.titleService.setTitle(this.pageTitle);
      this.identity = this._userService.getIdentity();
   }

   ngOnInit(){

   }

   ngDoCheck(){
      this.identity = this._userService.getIdentity();
   }

	goSearch(){
		console.log(this.search)
		this._router.navigate(['/result/'+this.search]);
	}
}
