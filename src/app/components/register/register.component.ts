import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { User } from '../../models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [ UserService ]
})
export class RegisterComponent implements OnInit {

   public pageTitle: string;
   public user: User;
   public status: string;
   constructor(
      private titleService: Title,
      private _userService: UserService
   ) {
      this.pageTitle = 'Register';
      this.titleService.setTitle(this.pageTitle);
      this.user = new User('','','','','','','ROLE_USER');
   }

   ngOnInit() {
   }

   onSubmit(form){
      this._userService.register(this.user).subscribe(
         response => {
            if(response.user && response.user._id){
               this.status = 'success';
               form.reset();
            }else{
               this.status = 'error';
            }
         },
         error => {
            console.log(error);
         }
      );
   }
}
