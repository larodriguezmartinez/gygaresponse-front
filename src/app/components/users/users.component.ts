import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from 'src/app/models/user';
import { global } from 'src/app/services/global';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

	public users: User[];
	public pageTitle: string;
	public urlCloudinary: string;
	public status: string;

	constructor(
		private _userService: UserService,
		private titleService: Title,

	) {
		this.pageTitle = 'User';
		this.titleService.setTitle(this.pageTitle);
		this.urlCloudinary = global.urlCloudinary;
	}

	ngOnInit() {
		this.getUsers();
	}

	getUsers(){
		this._userService.getUsers().subscribe(
			response => {
				if(response.users){
					this.users = response.users;
				}
			},
			error => {
				this.status = 'error';
			}
		);
	}
}
