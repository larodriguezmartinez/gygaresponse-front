import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ UserService ]
})
export class LoginComponent implements OnInit {

   public pageTitle: string;
   public user: User;
   public status: string;
   public identity: any;
   public token: string;

   constructor(
      private titleService: Title,
      private _userService: UserService,
      private _router: Router,
      private _route: ActivatedRoute
   ) {
      this.pageTitle = 'Login';
      this.titleService.setTitle(this.pageTitle);
      this.user = new User('','','','','','','ROLE_USER');

   }

   ngOnInit() {
   }

   onSubmit(form){
      this._userService.login(this.user).subscribe(
         response => {
            if(response.user && response.user._id){
               this.status = 'success';
               this.identity = response.user;
               localStorage.setItem('identity', JSON.stringify(this.identity) );
               this._userService.login(this.user, true).subscribe(
                  response => {
                     if(response.token){
                        this.token = response.token;
                        localStorage.setItem('token', this.token );
                        this.status = 'success';
                        this._router.navigate(['/home']);
                     }else{
                        this.status = 'error';
                     }
                  },
                  error => {
                     console.log(error);
                  }
               );
            }else{
               this.status = 'error';
            }
         },
         error => {
            this.status = 'error';
            console.log(error);
         }
      );
   }
}
