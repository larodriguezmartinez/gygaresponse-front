import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TopicService } from 'src/app/services/topic.service';
import { UserService } from 'src/app/services/user.service';
import { CommentService } from 'src/app/services/commet.service';
import { Topic } from 'src/app/models/topic';
import { Comment } from 'src/app/models/comment';
import { global } from 'src/app/services/global';

@Component({
	selector: 'app-topic-detail',
	templateUrl: './topic-detail.component.html',
	styleUrls: ['./topic-detail.component.css'],
	providers: [TopicService,UserService,CommentService]
})
export class TopicDetailComponent implements OnInit {

	public pageTitle: string;
	public status: string;
	public topic: Topic;
	public comment: Comment;
	public identity;
	public token;
	public urlCloudinary: string;

	constructor(
		private titleService: Title,
		private _route: ActivatedRoute,
		private _router: Router,
		private _topicService: TopicService,
		private _userService: UserService,
		private _commentService: CommentService,

	) {
		this.pageTitle = 'Topics';
		this.titleService.setTitle(this.pageTitle);
		this.identity = this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.comment = new Comment('', '', '', '');
		this.urlCloudinary = global.urlCloudinary;
	}

	ngOnInit() {
		this.getTopic();
	}

	getTopic(){

		this._route.params.subscribe( params => {

			let id = params['id'];

			this._topicService.getTopic(id).subscribe(
				response => {
					if(response.topic){

						this.topic = response.topic;
						this.topic.lang = response.topic.lang.toLowerCase();
					}else{
						this._router.navigate(['/home']);
					}
				},
				error => {
					this.status = 'error';
				}
			);
		});
	}

	onSubmit(form){

		this._commentService.addComment(this.token, this.comment, this.topic._id).subscribe(
			response => {
				if(!response.topic){
					this.status = 'error';
				}else{
					this.status = 'succcess';
					this.topic = response.topic;
					form.reset();
				}
			},
			error => {
				this.status = 'error';
			}
		);
	}

	deleteComment(id){
		this._commentService.deleteComment(this.token, id, this.topic._id).subscribe(
			response => {
				if(!response.topic){
					this.status = 'error';
				}else{
					this.status = 'succcess';
					this.topic = response.topic;
				}
			},
			error => {
				this.status = 'error';
			}
		);
	}
}
