import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TopicService } from 'src/app/services/topic.service';
import { Topic } from 'src/app/models/topic';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-search',
  templateUrl: '../topics/topics.component.html',
	styleUrls: ['./search.component.css'],
	providers: [TopicService]

})
export class SearchComponent implements OnInit {

	public pageTitle: string;
	public topics: Topic[];
	public statusSearch: string;
	public noPaginate;
	public topicsCount;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _topicService: TopicService,
		private titleService: Title,
	) {
		this.pageTitle = 'Results';
		this.titleService.setTitle(this.pageTitle);
		this.noPaginate = true;
	}

	ngOnInit() {
		this._route.params.subscribe( params => {
			var search = params['search'];
			this.pageTitle = 	this.pageTitle + ' '+ search;
			this.getTopics(search);
		})

	}

	getTopics(search){

		this._topicService.search(search).subscribe(
			response => {
				if(response.topics){
					this.topics = response.topics;
				}
			},
			error => {
				this.statusSearch = 'error';
			}
		);
	}
}
