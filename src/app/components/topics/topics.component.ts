import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TopicService } from 'src/app/services/topic.service';
import { Topic } from 'src/app/models/topic';
import { Title } from '@angular/platform-browser';

@Component({
	selector: 'app-topics',
	templateUrl: './topics.component.html',
	styleUrls: ['./topics.component.css'],
	providers: [TopicService]
})
export class TopicsComponent implements OnInit {

	public pageTitle: string;
	public topics: Topic[];
	public totalPages: Number;
	public page: Number;
	public nextPage: Number;
	public prevPage: Number;
	public numberPages: Array<Number>;
	public noPaginate;
	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _topicService: TopicService,
		private titleService: Title,
	) {
		this.pageTitle = 'Topics';
		this.titleService.setTitle(this.pageTitle);
		this.noPaginate = true;
	}

	ngOnInit() {
		this._route.params.subscribe( params => {
			var page = +params['page'];

			if(!page || page == null || page == undefined){
				page = 1;
				this.prevPage = 1;
				this.nextPage = 2;
			}
			this.getTopics(page);
		})

	}

	getTopics(page = 1){
		this._topicService.getTopics(page).subscribe(
			response => {
				if(response.topics){
					this.topics = response.topics;

					this.totalPages = response.totalPages;
					var numberPages = [];
					for (let i = 1; i <= this.totalPages; i++) {
						numberPages.push(i);
					}
					this.numberPages = numberPages;

					if(page >= 2){
						this.prevPage = page - 1;
					}else{
						this.prevPage = 1;
					}

					if(page < this.totalPages){
						this.nextPage = page + 1;
					}else{
						this.nextPage = this.totalPages;
					}

				}else{
					this._router.navigate(['/home']);
				}
			},
			error => {

			}
		);
	}
}
