import { Component, OnInit, OnChanges } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { global } from '../../services/global';
import { Response } from 'selenium-webdriver/http';

@Component({
  selector: 'app-uset-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css'],
  providers: [ UserService ]
})
export class UserEditComponent implements OnInit, OnChanges  {

   public pageTitle: string;
   public user: User;
   public identity: any;
   public token: string;
   public status: string;
   public fileUploaderConfig: any;
   public urlApi: string;
   public urlCloudinary: string;

   constructor(
      private titleService: Title,
      private _userService: UserService,
      private _router: Router,
      private _route: ActivatedRoute,
   ) {
      this.pageTitle = 'User settings';
      this.titleService.setTitle(this.pageTitle);
      this.identity = this._userService.getIdentity();
      this.token = this._userService.getToken();
      this.user = this.identity;
      this.urlApi = global.urlApi;
      this.urlCloudinary = global.urlCloudinary;
      this.fileUploaderConfig = {
         multiple: false,
         formatsAllowed: ".jpg, .jpeg, .gif, .png",
         maxSize: "2",
         theme: "attachPin",
         uploadAPI:{
            url: this.urlApi+'upload-avatar',
            headers : {
               "authorization": this.token,
            }
         },
         hideProgressBar: false,
         hideResetBtn: false,
         hideSelectBtn: false,
         replaceTexts: {
            selectFileBtn: 'Select Files',
            resetBtn: 'Reset',
            uploadBtn: 'Upload',
            dragNDropBox: 'Drag N Drop',
            attachPinBtn: 'Attach Files...',
            afterUploadMsg_success: 'Successfully Uploaded !',
            afterUploadMsg_error: 'Upload Failed !'
         }
      };
   }

   avatarUpload(data){

      let params = JSON.parse(data.response);
      this.user.image = params.user.image;

   }

   ngOnInit() {
   }

   ngOnChanges(){
   }

   onSubmit(form){
      this._userService.update(this.user).subscribe(
         response => {

            if(!response.user){
               this.status = 'error';
            }else{
               this.status = 'success';
               localStorage.setItem('identity', JSON.stringify(this.user));
            }
         },
         error => {
            this.status = 'error';
            console.log(error);
         }
      );
   }
}
