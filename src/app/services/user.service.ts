import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { global } from './global';

@Injectable()
export class UserService{

   public apiUrl: string;
   public identity: string;
   public token: string;

   constructor(private _http: HttpClient){
      this.apiUrl = global.urlApi;
   }

   register(user): Observable<any>{

      let headers = new HttpHeaders().set('Content-Type', 'application/json');

      return this._http.post(this.apiUrl+'register', user, {headers: headers});

   }

   login(user, getToken = null): Observable<any>{

      if(getToken != null ){
         user.gettoken = getToken;
      }

      let headers = new HttpHeaders().set('Content-Type', 'application/json');

      return this._http.post(this.apiUrl+'login', user, {headers: headers});

   }

   getIdentity(){

      let identity = JSON.parse(localStorage.getItem('identity'));

      if(identity && identity != null && identity != undefined && identity != "undefined"){
         this.identity = identity;
      }else{
         this.identity = null;
      }

      return this.identity;
   }

   getToken(){
      let token = localStorage.getItem('token');

      if(token && token != null && token != undefined && token != "undefined"){
         this.token = token;
      }else{
         this.token = null;
      }

      return this.token;
   }

   update(user):Observable<any>{

      let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                      .set('authorization', this.getToken());

      return this._http.put(this.apiUrl+'update', user, {headers: headers});
	}

	getUsers():Observable<any>{

		let headers = new HttpHeaders().set('Content-Type', 'application/json');

		return this._http.get(this.apiUrl+'users', {headers:headers})
	}

	getUser(userId):Observable<any>{

		let headers = new HttpHeaders().set('Content-Type', 'application/json');

		return this._http.get(this.apiUrl+'user/'+userId, {headers:headers})
	}
}
