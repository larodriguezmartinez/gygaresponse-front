import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../models/topic';
import { global } from './global';

@Injectable()
export class TopicService{

	public urlApi: string;

	constructor(
		private _http: HttpClient
	){
		this.urlApi = global.urlApi;
	}

	addTopic(token, topic):Observable<any>{

		let headers = new HttpHeaders().set('Content-type', 'application/json')
												.set('authorization', token);

		return this._http.post(this.urlApi+'topic', topic, { headers: headers });
	}

	getUserTopics(userId):Observable<any>{

		let headers = new HttpHeaders().set('Content-type', 'application/json');

		return this._http.get(this.urlApi+'user-topics/'+userId, { headers: headers });
	}

	getTopic(id):Observable<any>{
		let headers = new HttpHeaders().set('Content-type', 'application/json');

		return this._http.get(this.urlApi+'topic/'+id, { headers: headers });
	}

	updateTopic(token, id, topic):Observable<any>{

		let headers = new HttpHeaders().set('Content-type', 'application/json')
												.set('authorization', token);

		return this._http.put(this.urlApi+'topic/'+id, topic, { headers: headers });

	}

	deleteTopic(token, id ):Observable<any>{

		let headers = new HttpHeaders().set('Content-type', 'application/json')
												.set('authorization', token);

		return this._http.delete(this.urlApi+'topic/'+id, { headers: headers });

	}

	getTopics(page = 1 ):Observable<any>{
		let headers = new HttpHeaders().set('Content-type', 'application/json');

		return this._http.get(this.urlApi+'topics/'+page , { headers: headers });
	}

	search(string):Observable<any>{
		let headers = new HttpHeaders().set('Content-type', 'application/json');

		return this._http.get(this.urlApi+'search/'+string , { headers: headers });
	}

}