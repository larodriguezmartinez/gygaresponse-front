import { Injectable } from "@angular/core";
import { Router, CanActivate } from '@angular/router';
import { UserService } from './user.service';

@Injectable()
export class NoidentityGuards implements CanActivate{

	public identity: any;

	constructor(
		private _router: Router,
		private _userService: UserService,
	){}

	canActivate(){
		let identity = this._userService.getIdentity();

		if(identity){
			return false;
			this._router.navigate(['/home']);
		}else{
			return true;
		}
	}
}