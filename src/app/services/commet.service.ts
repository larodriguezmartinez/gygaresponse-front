import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../models/topic';
import { global } from './global';
import { Comment } from '../models/comment';

@Injectable()
export class CommentService{

	public urlApi: string;

	constructor(
		private _http: HttpClient
	){
		this.urlApi = global.urlApi;
	}

	addComment(token, comment,topicId):Observable<any>{

		let headers = new HttpHeaders().set('Content-type', 'application/json')
												.set('authorization', token);

		return this._http.post(this.urlApi+'comment/topic/'+topicId, comment, { headers: headers });
	}

	deleteComment(token, commentId, topicId):Observable<any>{

		let headers = new HttpHeaders().set('Content-type', 'application/json')
												.set('authorization', token);

		return this._http.delete(this.urlApi+'comment/'+topicId+'/'+commentId, { headers: headers });
	}
}