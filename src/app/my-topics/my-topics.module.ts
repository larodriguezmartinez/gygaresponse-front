import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MyTopicsRoutingModule } from './my-topics-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MomentModule } from 'angular2-moment';

import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { EditComponent } from './components/edit/edit.component';
import { ListComponent } from './components/list/list.component';
import { UserService } from '../services/user.service';
import { UserGuards } from '../services/user.guard';


@NgModule({
   declarations:[
      MainComponent,
      ListComponent,
      EditComponent,
      AddComponent
   ],
   imports:[
		CommonModule,
		FontAwesomeModule,
      FormsModule,
      HttpClientModule,
		MyTopicsRoutingModule,
		MomentModule
   ],
   exports:[
      MainComponent,
      ListComponent,
      EditComponent,
      AddComponent
   ],
   providers:[
		UserGuards,
		UserService
	]
})

export class MyTopicsModule {}
