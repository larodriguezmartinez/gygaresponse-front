import { NgModule } from "@angular/core";
import { RouterModule,Routes } from '@angular/router';

import { MainComponent } from './components/main/main.component';
import { AddComponent } from './components/add/add.component';
import { EditComponent } from './components/edit/edit.component';
import { ListComponent } from './components/list/list.component';
import { UserGuards } from '../services/user.guard';


const myTopicsRoutes: Routes = [
   {
      path: 'my-topics',
		component: MainComponent,
		canActivate: [UserGuards],
      children: [
         { path: '', component: ListComponent},
         { path: 'new', component: AddComponent},
         { path: 'list', component: ListComponent },
         { path: 'edit/:id' ,component: EditComponent }
      ]
   }
];

@NgModule({
   imports: [
      RouterModule.forChild(myTopicsRoutes)
   ],
   exports: [
      RouterModule
   ]
})

export class MyTopicsRoutingModule {}
