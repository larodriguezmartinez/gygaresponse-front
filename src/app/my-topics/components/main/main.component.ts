import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

    public pageTitle: string;

    constructor(
        private titleService: Title,

    ) {
        this.pageTitle = 'My Topics';
        this.titleService.setTitle(this.pageTitle);
    }

    ngOnInit() {
    }

}
