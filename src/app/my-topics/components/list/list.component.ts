import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params, Route, RouterEvent } from '@angular/router';
import { Topic } from 'src/app/models/topic';
import { UserService } from 'src/app/services/user.service';
import { TopicService } from 'src/app/services/topic.service';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.css'],
	providers:[ UserService, TopicService ]
})
export class ListComponent implements OnInit {

    public pageTitle: string;
    public topics: Array<Topic>;
    public identity;
    public token: String;
    public status: String;


	constructor(
		private titleService: Title,
		private _route: ActivatedRoute,
		private _router: Router,
		private _userSerivce: UserService,
		private _topicService: TopicService

	) {
		this.pageTitle = 'My Topics';
		this.titleService.setTitle(this.pageTitle);
		this.identity = this._userSerivce.getIdentity();
		this.token = this._userSerivce.getToken();
	}

	ngOnInit() {
		this.getUserTopics();
	}

	getUserTopics(){

		var userId = this.identity._id;

		this._topicService.getUserTopics(userId).subscribe(
			response => {
				if(response.topics){
					this.topics = response.topics
				}

			},
			error => {
				console.log(error);
				this.status = 'error';
			}
		);
	}

	deleteTopic(id){

		this._topicService.deleteTopic(this.token, id).subscribe(
			response => {
				this.getUserTopics();
			},
			error => {
				console.log(error);
				this.status = 'error';
			}
		);
	}
}
