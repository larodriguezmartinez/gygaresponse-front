import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params, Route, RouterEvent } from '@angular/router';
import { Topic } from 'src/app/models/topic';
import { UserService } from 'src/app/services/user.service';
import { TopicService } from 'src/app/services/topic.service';

@Component({
   selector: 'app-add',
   templateUrl: './add.component.html',
   styleUrls: ['./add.component.css'],
   providers:[ UserService, TopicService ]
})
export class AddComponent implements OnInit {

    public pageTitle: string;
    public topic: Topic;
    public identity;
    public token: String;
    public status: String;

    constructor(
		private titleService: Title,
		private _route: ActivatedRoute,
		private _router: Router,
		private _userSerivce: UserService,
		private _topicService: TopicService
    ) {
		this.pageTitle = 'New Topic';
		this.titleService.setTitle(this.pageTitle);
		this.identity = this._userSerivce.getIdentity();
		this.token = this._userSerivce.getToken();
		this.topic = new Topic('', '', '', '', '', '', this.identity.id, null);
    }

    ngOnInit() {

	}
	onSubmit(form){

		this._topicService.addTopic(this.token, this.topic).subscribe(
			response => {
				if(response.topic) {
					this.status = 'success';
					this.topic = response.topic;
					this._router.navigate(['/my-topics']);
				}
			},
			error => {
				console.log(error);
				this.status = 'error';
			}
		);
	}

}
