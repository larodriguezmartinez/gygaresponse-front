import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params, Route, RouterEvent } from '@angular/router';
import { Topic } from 'src/app/models/topic';
import { UserService } from 'src/app/services/user.service';
import { TopicService } from 'src/app/services/topic.service';

@Component({
    selector: 'app-edit',
    templateUrl: '../add/add.component.html',
	styleUrls: ['./edit.component.css'],
	providers:[ UserService, TopicService ]

})
export class EditComponent implements OnInit {

    public pageTitle: string;
    public topic: Topic ;
    public identity;
    public token: String;
    public status: String;

    constructor(
		private titleService: Title,
		private _route: ActivatedRoute,
		private _router: Router,
		private _userSerivce: UserService,
		private _topicService: TopicService

    ) {
        this.pageTitle = 'Edit Topic';
		this.titleService.setTitle(this.pageTitle);
		this.identity = this._userSerivce.getIdentity();
		this.token = this._userSerivce.getToken();
		this.topic = new Topic('', '', '', '', '', '', this.identity.id, null);
    }

    ngOnInit() {
		this.getTopic();
    }

	getTopic(){
		this._route.params.subscribe(params => {
			let id = params['id'];

			this._topicService.getTopic(id).subscribe(
				response => {
					if(!response.topic){
						this._router.navigate(['/my-topics']);
						this.status = 'error';
					}else{
						this.topic = response.topic;
					}
				},
				error => {
					this.status = 'error';
				}
			);
		});
	}
	onSubmit(form){

		var id = this.topic._id;

		this._topicService.updateTopic(this.token, id, this.topic).subscribe(
			response => {
				if(response.status === 'success' ){
					this.status = 'updated';
					// this.topic = response.topic;
				}else{
					this.status = 'error';
				}
			},
			error => {
				this.status = 'error';
			}
		);
	}
}
